#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <PubSubClient.h>
#include <Ethernet.h>
#include <SPI.h>


//Constantes de coneccion a WiFi
const char* ssid = "DCI";
const char* password = "DCI.2018";

//Constantes de servidor MQTT
const char* mqtt_server = "200.13.4.234";
const char* mqtt_topic = "iot/sonidomov";

//Objeto sensor DHT11
//DHT dht(D3, DHT11);
const int buttonPin = D4;
int milis;
int detect;

// clientes
WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;


void setup_wifi() {
  delay(100);
  Serial.print("Conectando a: ");
  Serial.println(ssid);
  WiFi.begin(ssid,password);
  while (WiFi.status() != WL_CONNECTED){
      delay(500);
      Serial.print(".");
  }
  randomSeed(micros());
  Serial.println("");
  Serial.println("Coneccion WiFi realizada");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

}

void callback(char* topic, byte* payload, unsigned int length){
  Serial.print("Command is : [");
  Serial.print(topic);
  int p =(char)payload[0]-'0';
  // if MQTT comes a 0 message, show humidity
  if(p==0)
  {
 //
  }
  // if MQTT comes a 1 message, show temperature
  if(p==1)
  {
  // digitalWrite(BUILTIN_LED, HIGH);
  }
  Serial.println();
} //end callback

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected())
  {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    //if you MQTT broker has clientID,username and password
    //please change following line to    if (client.connect(clientId,userName,passWord))
    if (client.connect(clientId.c_str()))
    {
      Serial.println("connected");
     //once connected to MQTT broker, subscribe command if any
      client.subscribe(mqtt_topic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 6 seconds before retrying
      delay(6000);
    }
  }
} //end reconnect()

void setup() {
  Serial.begin(9600);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  
  Serial.begin(9600);           // open serial port, set the baud rate to 9600 bps
  pinMode(buttonPin, INPUT); 
  milis = millis();
  detect = milis;

  Serial.println("Setup!");
}

void loop() {
  char message[58];
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
 
      //publish sensor data to MQTT broker
    client.publish(mqtt_topic, "Test");
 
    int val;
    String data = "";
    String movimiento = "false";
    String ocupado = "true";
    val=analogRead(A0); 
    milis = millis();
    
    data = data + "{\"usr\":\"iot\",\"pass\":\"iot.2018\",\"action\":\"create\",";
    
    if (digitalRead(buttonPin) == HIGH) {
      detect = milis;
      movimiento = "true";
    }else if (val>15){
      detect = milis;
    }

    if (detect<milis-15000){
      ocupado = "false";
    }
    
    data = data + "\"ocupado\": ";
    data = data + ocupado + ",";
    data = data + "\"sonido\": ";
    data = data + val + ",";
    data = data + "\"movimiento\": ";
    data = data + movimiento + ",";
    data = data + "\"sensor\": ";
    data = data + "2" + "}";

    if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status

      HTTPClient http;    //Declare object of class HTTPClient

      http.begin("http://200.13.4.234/index.php?eID=amb_registro");
      http.addHeader("Content-Type", "application/x-www-form-urlencoded");

      int httpCode = http.POST(data);   //Send the request
      String payload = http.getString();                  //Get the response payload

      Serial.println(httpCode);   //Print HTTP return code
      Serial.println(payload);    //Print request response payload

      http.end();  //Close connection

    }else{

      Serial.println("Error in WiFi connection");
    }

    Serial.println(data);
    Serial.print("Response body from server: ");
    delay(5000);
  
  }


